#include <stdio.h>
#include <stdlib.h>

#include "Exam.h"

int main(int argc, char *argv[])
{

    printf("Prime numbers\n");
    if (argc==0)
        return EXIT_SUCCESS;

    return prime(argc, argv);
}
